/*
 * Op.swift
 *
 *
 * Created by François Lamboley on 17/10/2019.
 */

import Foundation



class AsyncOp : Operation {
	
	private let queue = DispatchQueue(label: "async.operation.queue")
	private let lock = NSLock()
	
	private var _executing = false
	private var _finished = false
	
	override var isExecuting: Bool {
		get {
			lock.lock()
			let wasExecuting = _executing
			lock.unlock()
			return wasExecuting
		}
		set {
			if isExecuting != newValue {
				willChangeValue(forKey: "isExecuting")
				lock.lock()
				_executing = newValue
				lock.unlock()
				didChangeValue(forKey: "isExecuting")
			}
		}
	}
	
	override var isFinished: Bool {
		get {
			lock.lock()
			let wasFinished = _finished
			lock.unlock()
			return wasFinished
		}
		set {
			if isFinished != newValue {
				willChangeValue(forKey: "isFinished")
				lock.lock()
				_finished = newValue
				lock.unlock()
				didChangeValue(forKey: "isFinished")
			}
		}
	}
	
	override var isAsynchronous: Bool {
		return true
	}
	
	override func start() {
		if isCancelled {
			isFinished = true
			return
		}
		
		isExecuting = true
		
		queue.async {
			Thread.sleep(forTimeInterval: 1)
			print("async done (should be first)")
			self.isExecuting = false
			self.isFinished = true
		}
	}
	
}
