import Foundation

let q = OperationQueue()
q.maxConcurrentOperationCount = 42



let asyncOp = AsyncOp()
let syncOp = BlockOperation{ Thread.sleep(forTimeInterval: 1); print("sync done (should be second)") }

syncOp.addDependency(asyncOp)
q.addOperations([syncOp, asyncOp], waitUntilFinished: true)


let asyncOpRetrying = AsyncOpRetrying()
let syncOpRetrying = SyncOpRetrying()

print("yo")
syncOpRetrying.addDependency(asyncOpRetrying)
q.addOperations([syncOpRetrying, asyncOpRetrying], waitUntilFinished: true)
