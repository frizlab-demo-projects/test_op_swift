/*
 * Op.swift
 *
 *
 * Created by François Lamboley on 17/10/2019.
 */

import Foundation

import RetryingOperation



class AsyncOpRetrying : RetryingOperation {
	
	override func startBaseOperation(isRetry: Bool) {
		DispatchQueue(label: "yo").asyncAfter(deadline: .now() + .seconds(1), execute: {
			print("async retrying done (should be first)")
			self.baseOperationEnded()
		})
	}
	
	override var isAsynchronous: Bool {
		return true
	}
	
}

class SyncOpRetrying : RetryingOperation {
	
	override func startBaseOperation(isRetry: Bool) {
		Thread.sleep(forTimeInterval: 1)
		print("sync retrying done (should be second)")
		baseOperationEnded()
	}
	
	override var isAsynchronous: Bool {
		return false
	}
	
}
