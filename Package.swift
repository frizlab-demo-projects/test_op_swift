// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.
import PackageDescription


let package = Package(
	name: "test_op",
	dependencies: [
		.package(url: "https://github.com/happn-app/RetryingOperation.git", from: "1.1.6"),
	],
	targets: [
		.target(name: "test_op", dependencies: ["RetryingOperation"])
	]
)
