#!/usr/bin/swift

import Foundation

let queue = OperationQueue()
queue.maxConcurrentOperationCount = 2

for _ in 0..<2 {
	var results = [Int]()

	let op1 = BlockOperation{           results.append(1) }
	let op2 = BlockOperation{ sleep(1); results.append(2) }
	op1.addDependency(op2)

	queue.addOperations([op1, op2], waitUntilFinished: true)

	let expected = [2, 1]
	if results != expected {
		print("Got:      \(results)")
		print("Expected: \(expected)")
	} else {
		print("ok")
	}
}
